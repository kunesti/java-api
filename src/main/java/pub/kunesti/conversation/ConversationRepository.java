package pub.kunesti.conversation;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import pub.kunesti.event.Event;


public interface ConversationRepository extends JpaRepository<Conversation, Long> {

    Page<Conversation> findAllById(Long id, Pageable pageable);

    Conversation findFirstByEvent(Event event);

}
