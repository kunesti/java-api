package pub.kunesti.conversation;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class ConversationDTO {

    private Long id;

    @NotNull
    private String message;

    @NotNull
    private Long event;

}
