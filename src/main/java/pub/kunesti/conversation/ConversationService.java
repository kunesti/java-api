package pub.kunesti.conversation;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pub.kunesti.event.Event;
import pub.kunesti.event.EventRepository;
import pub.kunesti.model.SimplePage;
import pub.kunesti.util.NotFoundException;


@Service
public class ConversationService {

    private final ConversationRepository conversationRepository;
    private final EventRepository eventRepository;

    public ConversationService(final ConversationRepository conversationRepository,
            final EventRepository eventRepository) {
        this.conversationRepository = conversationRepository;
        this.eventRepository = eventRepository;
    }

    public SimplePage<ConversationDTO> findAll(final String filter, final Pageable pageable) {
        Page<Conversation> page;
        if (filter != null) {
            Long longFilter = null;
            try {
                longFilter = Long.parseLong(filter);
            } catch (final NumberFormatException numberFormatException) {
                // keep null - no parseable input
            }
            page = conversationRepository.findAllById(longFilter, pageable);
        } else {
            page = conversationRepository.findAll(pageable);
        }
        return new SimplePage<>(page.getContent()
                .stream()
                .map((conversation) -> mapToDTO(conversation, new ConversationDTO()))
                .toList(),
                page.getTotalElements(), pageable);
    }

    public ConversationDTO get(final Long id) {
        return conversationRepository.findById(id)
                .map(conversation -> mapToDTO(conversation, new ConversationDTO()))
                .orElseThrow(NotFoundException::new);
    }

    public Long create(final ConversationDTO conversationDTO) {
        final Conversation conversation = new Conversation();
        mapToEntity(conversationDTO, conversation);
        return conversationRepository.save(conversation).getId();
    }

    public void update(final Long id, final ConversationDTO conversationDTO) {
        final Conversation conversation = conversationRepository.findById(id)
                .orElseThrow(NotFoundException::new);
        mapToEntity(conversationDTO, conversation);
        conversationRepository.save(conversation);
    }

    public void delete(final Long id) {
        conversationRepository.deleteById(id);
    }

    private ConversationDTO mapToDTO(final Conversation conversation,
            final ConversationDTO conversationDTO) {
        conversationDTO.setId(conversation.getId());
        conversationDTO.setMessage(conversation.getMessage());
        conversationDTO.setEvent(conversation.getEvent() == null ? null : conversation.getEvent().getId());
        return conversationDTO;
    }

    private Conversation mapToEntity(final ConversationDTO conversationDTO,
            final Conversation conversation) {
        conversation.setMessage(conversationDTO.getMessage());
        final Event event = conversationDTO.getEvent() == null ? null : eventRepository.findById(conversationDTO.getEvent())
                .orElseThrow(() -> new NotFoundException("event not found"));
        conversation.setEvent(event);
        return conversation;
    }

}
