package pub.kunesti.user;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import pub.kunesti.event.Event;


public interface UserRepository extends JpaRepository<User, Long> {

    Page<User> findAllById(Long id, Pageable pageable);

    boolean existsByUserNameIgnoreCase(String userName);

    boolean existsByEmailIgnoreCase(String email);

    User findFirstByEvents(Event event);

}
