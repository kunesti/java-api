package pub.kunesti.conversation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import pub.kunesti.config.BaseIT;


public class ConversationResourceTest extends BaseIT {

    @Test
    @Sql({"/data/eventData.sql", "/data/conversationData.sql"})
    void getAllConversations_success() throws Exception {
        mockMvc.perform(get("/api/conversations")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.totalElements").value(2))
                .andExpect(jsonPath("$.content[0].id").value(((long)1200)));
    }

    @Test
    @Sql({"/data/eventData.sql", "/data/conversationData.sql"})
    void getAllConversations_filtered() throws Exception {
        mockMvc.perform(get("/api/conversations?filter=1201")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.totalElements").value(1))
                .andExpect(jsonPath("$.content[0].id").value(((long)1201)));
    }

    @Test
    @Sql({"/data/eventData.sql", "/data/conversationData.sql"})
    void getConversation_success() throws Exception {
        mockMvc.perform(get("/api/conversations/1200")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message").value("Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat."));
    }

    @Test
    void getConversation_notFound() throws Exception {
        mockMvc.perform(get("/api/conversations/1866")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.exception").value("NotFoundException"));
    }

    @Test
    @Sql("/data/eventData.sql")
    void createConversation_success() throws Exception {
        mockMvc.perform(post("/api/conversations")
                        .accept(MediaType.APPLICATION_JSON)
                        .content(readResource("/requests/conversationDTORequest.json"))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
        assertEquals(1, conversationRepository.count());
    }

    @Test
    void createConversation_missingField() throws Exception {
        mockMvc.perform(post("/api/conversations")
                        .accept(MediaType.APPLICATION_JSON)
                        .content(readResource("/requests/conversationDTORequest_missingField.json"))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.exception").value("MethodArgumentNotValidException"))
                .andExpect(jsonPath("$.fieldErrors[0].field").value("message"));
    }

    @Test
    @Sql({"/data/eventData.sql", "/data/conversationData.sql"})
    void updateConversation_success() throws Exception {
        mockMvc.perform(put("/api/conversations/1200")
                        .accept(MediaType.APPLICATION_JSON)
                        .content(readResource("/requests/conversationDTORequest.json"))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        assertEquals("Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.", conversationRepository.findById(((long)1200)).get().getMessage());
        assertEquals(2, conversationRepository.count());
    }

    @Test
    @Sql({"/data/eventData.sql", "/data/conversationData.sql"})
    void deleteConversation_success() throws Exception {
        mockMvc.perform(delete("/api/conversations/1200")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
        assertEquals(1, conversationRepository.count());
    }

}
